# Informações uteis

## Database
mongodb+srv://dbuser:<password>@suaaplicacao-dev-nmat5.mongodb.net/test?retryWrites=true
userdb: userdb

## Github
Client ID: 83d8c9d59b962e59128d
Client Secret: 0c51887cb08f83ae8a8b4e43393a619e7fa68d22

## Docker
Comandos utilizados:

```
docker build -t shandersonvieira/backend-scm:alpha
docker tag <docker-id> shandersonvieira/backend-scm:alpha-2
docker login
docker push shandersonvieira/backend-scm:alpha
```